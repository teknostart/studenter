[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/teknostart/studenter) 

# Introduksjon til gruppearbeidsdelen av Teknostart 2023

Dere har nå vært gjennom et lite Git-kurs, og i gruppearbeidsdelen av Teknostart 2023 skal dere få praktisert litt av det dere lærte! Men det er ikke Git som er hovedhensikten med denne delen, det er nemlig å:

- bli kjent med andre i kullet i studieprogrammet ditt
- bli litt kjent med fagmiljøet på instituttet
- reflektere litt over et IT-relatert samfunnsspørsmål

Dette skal skje ved at dere får en rekke temaer å velge mellom, og så skal dere lage en digital plakat (det vi kaller en "poster", som passer til å bli hengt opp på en "stand") og skrive litt om bakgrunnen for den.

Leveransen er et sett filer (spesifisert under) som skal legges på et eget gruppe-repo, som læringsassistene lager for dere. Dere kan ta utgangspunkt i innholdet i dette repoet og bruke egnede Git-kommandoer for å få dem over (Hint: bytt "remote").

Bytt gjerne ut denne README-fila med en som er bedre, f.eks. med navn på gruppemedlemmene, gruppebilde og lenke til [bakgrunnsdokumentet](bakgrunn/README.md).

Det er fint om dere beholder "Gitpod Ready-to-Code"-merkelappen øverst, så gitpod kan åpnes på deres repo.

Link til slides fra git-kurs kan finnes [her](https://s.ntnu.no/ts23). 

## Plakaten

Den digitale plakaten er det mest synlig resultatet av gruppearbeidet. Dere står helt fritt i valg av form og innhold,
så lenge det representerer gruppearbeidet på en god måte. Det kan være både informativt, reflekterende, argumenterende osv. Det er f.eks. helt greit at ulike perspektiver illustreres, og en trenger ikke konkludere med et svar på spørsmålet.

Plakaten leveres som en fil i repoet, som en pdf med navn "plakat.pdf", i samme mappe som denne fila. Kilden kan være hva som helst, f.eks. Powerpoint eller en nettbasert app, bare det leveres som en pdf.

## Tekst om bakgrunnen for plakaten

Bakgrunnen for plakaten leveres som et sett med sammenkoblede markdown-filer, slik at en ved å lese dem får innblikk i prosessen dere har vært gjennom. Disse skal ligge i mappa bakgrunn og hovedfila skal hete [README.md](bakgrunn/README.md), slik at den kommer opp med en gang, hvis en navigerer til mappa i Gitlab. Akkurat hvilke filer dere deler opp i er ikke så farlig, men lista over kilder/referanser, skal ligge i en egen fil, [referanser.md](bakgrunn/referanser.md), og lenkes til fra hovedfila.

## Temaforslag

Vi har laget forslag til temaer, med en påstand for hver av dem:

**Sosiale medier**

- Sosiale mediums rolle i BLM-bevegelsen: "Uten sosiale medier, ingen BLM-bevegelse". [Link](https://www.nettavisen.no/norsk-debatt/profilbilde-aksjoner-er-ikke-nyttelose/o/5-95-258438) kan brukes til inspirasjon.
- Falske nyheter og desinformasjon i sosiale media: "Kan Facebook hjernevaske deg?". [Link](https://teknologiradet.no/korona-krigen-pagar-for-fullt-i-sosiale-medier/) kan brukes til inspirasjon.
-  Sosiale medium og lykke: "Gjør sosiale medier livet bedre?". [Link](https://ung.forskning.no/fritid-media-psykologi/du-kan-bli-trist-og-negativ-av-a-se-lykkelige-sommerbilder-pa-sosiale-medier/1890109) kan brukes til inspirasjon.



**Kunstig intelligens**

- Fører kunstig intelligens til færre arbeidsplasser for mennesker? "Nei til robotisering, vi mennesker må ha noe å gjøre!". [Link](https://www.tu.no/artikler/fersk-studie-robotisering-vil-gi-faerre-jobber-og-lavere-lonn/396472) kan brukes til inspirasjon.
- Diskriminerende kunstig intelligens: "Skjevhet i kunstig intelligente systemer øker strukturell diskriminering". [Link](https://forskersonen.no/debattinnlegg-kunstig-intelligens-meninger/for-enkelt-om-kunstig-intelligens--diskriminerende-og-fordomsfull-ai-er-ikke-alltid-lett-a-lose/1777178) kan brukes til inspirasjon.
- Overvåking av befolkningen med hjelp av kunstig intelligens: "Kunstig intelligens, en gavepakke til totalitære og udemokratiske regimer!". [Link](https://www.politiforum.no/ansiktsgjenkjenning-kronikk/ansiktsgjenkjenning-ny-teknologi-kaster-om-pa-tradisjonelle-forestillinger/158800) kan brukes til inspirasjon.



**IT og samfunn**

- Digitalisering og ny teknologi gir nye muligheter, men også trusler. "Digitalisering av offentlige tjenester, løsning eller trussel?". [Link](https://www.dagsavisen.no/debatt/2021/07/06/pa-tide-med-nye-perspektiver/) kan brukes til inspirasjon.
- Automatisering og robotisering av helsevesenet. "Vi trenger varme hender, ikke kalde maskiner". [Link](https://www.youtube.com/watch?v=K7BLnLqWGj4) kan brukes til inspirasjon. 
- Automatisering av IT-yrket "Fjerner kunstig intelligens også jobben som IT-utvikler?". [Link](https://copilot.github.com/) kan brukes til inspirasjon. 
- Grønn IT og datasentre. "Unyttige skytjenester drevet av skitten energi ødelegger miljøet". [Link](https://www.digi.no/artikler/debatt-gronn-drift-eller-gronnvasking/503653) kan brukes til inspirasjon. 
- Smarte byer. "IT og AI er løsningen på bærekraftig urbanitet". [Link](https://www.trondheim.kommune.no/trondheim-blir-smartby/) kan brukes til inspirasjon.
- Dekning av IT-profesjonen i dagens medier, hva tror folk om IT-profesjonelle? "IT-utviklere er nerder som ikke forstår brukerne". [Link](https://www.digi.no/artikler/debatt-vi-ma-normalisere-programmering/506202) kan brukes til inspirasjon.

## Forslag til tekst om bakgrunnen

Bakgrunnsteksten må dekke følgende:

- Hvorfor dere valgte temaet
- Hvem på IDI dere tror kan noe om dette
- Referanser til kilder (i en egen fil)

I tillegg har vi noen forslag til ting det kan være lurt å ha med, om ikke annet for å hjelpe
dere å strukturere prosessen:

- Begrepsdefinisjoner - hvordan definerer dere ulike begreper knyttet til temaet?
- Antakelser som ligger til grunn for påstanden
- Hva dere legger i temaet, basert på *egne* erfaringer
- Hva er nytt ved temaet, hvorfor er det aktuelt?
- Relevans og viktighet av temaet
- Positive og negative aspekter
- Eksempler
